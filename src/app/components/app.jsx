import React from 'react';

class MainApp extends React.Component {

  render() {
    return (
      <div>
        <h1>Hello React!</h1>
      </div>
    );
  }

}

export default MainApp;
