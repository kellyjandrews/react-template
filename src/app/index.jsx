import React from 'react';
import ReactDOM from 'react-dom';
import MainApp from './components/app.jsx';

ReactDOM.render(
  <MainApp />,
  document.getElementById('MainApp')
);
