import express from 'express';
import path from 'path';
// import bodyParser from 'body-parser';
// import cors from 'cors';
import ErrorHandler from 'express-simple-errors';
// import routes from './api/todos/routes';

const app = express();
const port = process.env.PORT || 3000;
const errorHandler = new ErrorHandler();

// app middleware
// app.use(cors());
// app.use(bodyParser.json({type: 'application/json'}));
app.disable('etag');

app.use(express.static(path.join(__dirname, 'public')));


// validation errors are not typed correctly - changing here
app.use((err, req, res, next) => {
  if (err.validator) {
    err.name = 'ValidationError';
    err.code = 400;
  }
  next(err);
});

errorHandler.setHandler('ValidationError', function validationHandler (err, stack) {
  const res = {};
  res.status = 'Error';
  res.message = `Schema Validation Error: ${err.message}`;
  res.code = err.code;
  if (stack) res.stackTrace = err.stack;
  return res;
});

app.use(errorHandler.middleware());

app.listen(port, () => console.log(`Your app is running on port ${port}`)); // eslint-disable-line
